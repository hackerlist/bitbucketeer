#!/usr/bin/env python
#-*- coding: utf-8 -*-

import argparse
import requests
from rauth import OAuth1Service, OAuth1Session

class Bitbucket:

    # API URLs https://confluence.atlassian.com/display/BITBUCKET/oauth+Endpoint
    BASE_API = 'https://api.bitbucket.org'
    REQUEST_TOKEN_URL = 'https://bitbucket.org/!api/1.0/oauth/request_token'
    ACCESS_TOKEN_URL = 'https://bitbucket.org/!api/1.0/oauth/access_token'
    AUTHORIZE_URL = 'https://bitbucket.org/!api/1.0/oauth/authenticate'
    SERVICE = 'bitbucket'

    def __init__(self, username, consumer_key, consumer_secret,
                 oauth_token="", oauth_token_secret="",
                 callback_url='http://localhost?dump'):
        self.username = username

        if oauth_token and oauth_token_secret:
            self.session = OAuth1Session(consumer_key, consumer_secret,
                                         oauth_token, oauth_token_secret)
        else:
            self.session = self.oauth(consumer_key, consumer_secret,
                                      callback_url=callback_url)

    @property
    def _consumer(self):
        return self.session.consumer_key, self.session.consumer_secret

    @property
    def _oauth(self):
        return self.session.access_token, self.session.access_token_secret

    @classmethod
    def oauth_service(cls, consumer_key, consumer_secret):
        """Create a bitbucket auth service for OAuth 1.0"""
        return OAuth1Service(name=cls.SERVICE,
                             consumer_key=consumer_key,
                             consumer_secret=consumer_secret,
                             request_token_url=cls.REQUEST_TOKEN_URL,
                             access_token_url=cls.ACCESS_TOKEN_URL,
                             authorize_url=cls.AUTHORIZE_URL)

    @classmethod
    def oauth(cls, consumer_key, consumer_secret, 
              callback_url='http://localhost?dump'):
        """Performed OAuth for a new user, requires the browser + user
        intervention, returns session
        """
        service = cls.oauth_service(consumer_key, consumer_secret)
        params = {'oauth_callback': callback_url}
        rtoken, rtoken_secret = service.get_request_token(params=params)
        authorize_url = service.get_authorize_url(rtoken)
        print authorize_url
        oauth_verifier = raw_input('Enter oauth_verifier here: ')
        data = {'oauth_verifier': oauth_verifier}
        session = service.get_auth_session(rtoken, rtoken_secret, data=data)
        return session

    def get(self, *args, **kwargs):
        return self.session.get(*args, **kwargs)

    def delete(self, *args, **kwargs):
        return self.session.delete(*args, **kwargs)

    def put(self, *args, **kwargs):
        return self.session.put(*args, **kwargs)

    def invite(self, recipient, accountname, repo, permission="read"):
        """Invite user to repository by email (or id?)"""
        params = {'permission': permission}
        uri = '%s/1.0/invitations/%s/%s/%s' % \
            (self.BASE_API, accountname, repo, recipient)
        return self.session.post(uri, data=params)

    def create_team(self, accountname, group_owner, group_name, privilege="read"):
        """Create a new team/org"""
        params = {'kind': 'team',
                  'privilege': permission}
        uri = '%s/1.0/users/%s/privileges/%s/%s' % \
            (self.BASE_API, accountname, group_owner, group_name)
        return self.session.post(uri, data=params)

    def create_repo(self, accountname, repo_slug, name="", scm="git",
                    is_private=True, description="",
                    forking_policy="no_public_forks", language="", has_issues=True,
                    has_wiki=True):
        """Shortcut for creating repositories"""
        params = {'name': name or repo_slug,
                  'description': description,
                  'forking_policy': forking_policy,
                  'language': language,
                  'has_issues': has_issues,
                  'has_wiki': has_wiki
                  }
        uri = '%s/2.0/repositories/%s/%s' % (self.BASE_API, accountname, repo_slug)
        return self.session.post(uri, data=params)

def retrieve_oauth_tokens(username, consumer_key, consumer_secret):
    """retrieve_oauth_tokens offers a mechanism for manually
    retrieving your bitbucket oauth tokens/credentials, in the event
    you would like to use oauth to manage your own account (as an
    alternative to username/password).
    """
    b = Bitbucket(username, consumer_key, consumer_secret)
    return b._oauth


def argparser():
    parser = argparse.ArgumentParser(description="Bitbucket OAuth 1.0")
    parser.add_argument("-u", "--username", type=str, default='',
                        help="Bitbucket Username or Email")
    parser.add_argument("-k", "--consumer_key", type=str, default='',
                        help="Consumer Key")
    parser.add_argument("-s", "--consumer_secret", type=str, default='',
                        help="Consumer Secret")
    parser.add_argument("--oauth_token", type=str, default='',
                        help="OAuth Token")
    parser.add_argument("--oauth_token_secret", type=str, default='',
                        help="OAuth Secret Token")
    return parser

if __name__ == "__main__":
    parser = argparser()
    args = parser.parse_args()

    if not (args.consumer_key and args.consumer_secret and args.username):
        raise AttributeError("required args: username, consumer_key, " \
                                 "and consumer_secret")
    # xor; if one or the other
    if bool(args.oauth_token) ^ bool(args.oauth_token_secret):
        raise AttributeError("oauth_token and oauth_token_secret must " \
                            "be both absent or present")

    b = Bitbucket(args.username, args.consumer_key, args.consumer_secret,
                  oauth_token=args.oauth_token,
                  oauth_token_secret=args.oauth_token_secret)
    
    r = b.get('%s/1.0/users/%s' % (Bitbucket.BASE_API, args.username))
    print r.status_code
    print r.json()