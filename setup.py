#-*- coding: utf-8 -*-

"""
    bitbucketeer
    ~~~~~~~~~~~~

    Setup
    `````

    $ pip install foo
"""

from distutils.core import setup
import os

setup(
    name='bitbucketeer',
    version='0.0.01',
    url='',
    author='mek',
    author_email='m@hackerlist.net',
    packages=[
        'foo',
        ],
    platforms='any',
    license='LICENSE',
    install_requires=[
        'rauth >= 0.6.2'
    ],
    scripts=[
        ""
        ],
    description="",
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
)
