# Bitbuckerteer

Connect, manage, and automate your bitbucket account for great justice
using OAuth.

## Install / Setup

    $ sudo pip install .

## Usage

    # Pre-oauth    
    b = Bitbucket('email@provider.tld', <consumer_key>, <consumer_secret>)
    b._oauth # your tokens
    (<oauth_token>, <oauth_token_secret>)

    # Post-oauth
    b2 = Bitbucket('email@provider.tld', <consumer_key>, <consumer_secret>,
                   <oauth_token>, <oauth_token_secret>)

## Command Line Example

    # Walks you through the steps of retrieving your OAuth token credentials
    $ python bitbucketeer.py -u email@provider.tld -k <consumer_key> -s <consumer_secret>

    # Or, test your consumer and oauth tokens if you already have them
    # both oauth_token and oauth_token_secret must be provided or omitted
    $ python bitbucketeer.py -u email@provider.tld -k <consumer_key> -s <consumer_secret> --oauth_token <oauth_token> --oauth_token_secret <oauth_token_secret>